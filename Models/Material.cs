﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialsApp.Models
{
    public class Material
    {
        public int ID { get; set; }

        [Required]
        public string ErpCode { get; set; }

        public string Description { get; set; }


        [Range (1, 10000000000)]
         
        public int Stock { get; set; }

    }
}
